package com.ivanmix.work.entity;

import java.sql.Date;

public class Worker {
    private Long id;
    private String name;
    private String email;
    private int age;
    private Date birthday;
    private Long departmentId;

    public Worker() { }

    public Worker(String name, String email, int age, Date birthday, Long departmentId) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.birthday = birthday;
        this.departmentId = departmentId;
    }

    public Worker(Long id, String name, String email, int age, Date birthday, Long departmentId) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.age = age;
        this.birthday = birthday;
        this.departmentId = departmentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public String toString() {
        return "Worker{"
                + "id=" + id
                + ", name='" + name + '\''
                + ", email='" + email + '\''
                + ", age=" + age
                + ", birthday=" + birthday
                + ", departmentId=" + departmentId
                + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Worker worker = (Worker) o;
        return email != null ? email.equals(worker.email) : worker.email == null;
    }

    @Override
    public int hashCode() {
        return email != null ? email.hashCode() : 0;
    }
}
