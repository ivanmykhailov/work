package com.ivanmix.work.servlets;

import com.ivanmix.work.entity.Worker;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.helper.HelperServlet;
import com.ivanmix.work.service.WorkerService;
import com.ivanmix.work.service.impl.WorkerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WorkerDeleteServlet extends HttpServlet {
    WorkerService workerService = new WorkerServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long workerId = HelperServlet.getId(req);
            Worker worker = workerService.getWorkerById(workerId);
            req.setAttribute("worker",  worker);
            req.getRequestDispatcher("/WEB-INF/views/workerDelete.jsp").forward(req, resp);
        } catch (EntityNotExist workerIsNotExist) {
            req.setAttribute("message", "Worker does not exist");
            req.getRequestDispatcher("/WEB-INF/views/404.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            try {
                Long id = Long.valueOf(req.getParameter("id"));
                Worker worker = workerService.getWorkerById(id);
                workerService.delete(id);
                resp.sendRedirect(String.format("%s/department/%s", req.getContextPath(), worker.getDepartmentId()));
            } catch (NumberFormatException e) {
                throw new EntityNotExist();
            }
        } catch (EntityNotExist workerIsNotExist) {
            req.setAttribute("message", "Worker does not exist");
            req.getRequestDispatcher("/WEB-INF/views/404.jsp").forward(req, resp);
        }
    }
}
