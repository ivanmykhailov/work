package com.ivanmix.work.servlets;

import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.helper.HelperServlet;
import com.ivanmix.work.service.WorkerService;
import com.ivanmix.work.service.impl.WorkerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DepartmentServlet extends HttpServlet {
    private WorkerService workerService = new WorkerServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long departmentId = HelperServlet.getId(req);
            req.setAttribute("workers", workerService.getWorkersByDepartmentId(departmentId));
            req.getRequestDispatcher("/WEB-INF/views/workerList.jsp").forward(req, resp);
        } catch (EntityNotExist workerIsNotExist) {
            req.setAttribute("message", "Department does not exist");
            req.getRequestDispatcher("/WEB-INF/views/404.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(String.format("%s/departments", req.getContextPath()));
    }
}
