package com.ivanmix.work.servlets;

import com.ivanmix.work.entity.Worker;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.helper.HelperServlet;
import com.ivanmix.work.helper.HelperWorkerServlet;
import com.ivanmix.work.service.DepartmentService;
import com.ivanmix.work.service.WorkerService;
import com.ivanmix.work.service.impl.DepartmentServiceImpl;
import com.ivanmix.work.service.impl.WorkerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WorkerEditServlet extends HelperWorkerServlet {
    private WorkerService workerService = new WorkerServiceImpl();
    private DepartmentService departmentService = new DepartmentServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long workerId = HelperServlet.getId(req);
            Worker worker = workerService.getWorkerById(workerId);
            req.setAttribute("worker",  worker);
            req.setAttribute("departments", departmentService.getAllDepartments());
            req.setAttribute("formUrl", "edit");
            req.getRequestDispatcher("/WEB-INF/views/workerAddEdit.jsp").forward(req, resp);
        } catch (EntityNotExist workerIsNotExist) {
            req.setAttribute("message", "Worker does not exist");
            req.getRequestDispatcher("/WEB-INF/views/404.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        workerAddEdit(req, resp, "edit");
    }
}
