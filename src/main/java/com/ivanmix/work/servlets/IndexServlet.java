package com.ivanmix.work.servlets;

import com.ivanmix.work.dao.impl.JdbcConfig;
import com.ivanmix.work.service.DepartmentService;
import com.ivanmix.work.service.impl.DepartmentServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


public class IndexServlet  extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(IndexServlet.class);
    private DepartmentService departmentService = new DepartmentServiceImpl();

    @Override
    public void init() {
        try {
            JdbcConfig.initProject();
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        LOGGER.debug("init");
    }

    @Override
    public void destroy() {
        LOGGER.debug("destroy");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestURI = req.getRequestURI();
        if (requestURI.length() > 1 && requestURI.indexOf("Work-1.1-SNAPSHOT") != 1) {
            req.setAttribute("message", "Page not found");
            req.getRequestDispatcher("/WEB-INF/views/404.jsp").forward(req, resp);
        }
        req.setAttribute("departments", departmentService.getAllDepartments());
        req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(String.format("%s", req.getContextPath()));
    }
}
