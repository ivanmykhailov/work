package com.ivanmix.work.servlets;

import com.ivanmix.work.helper.HelperDepartmentServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DepartmentAddServlet extends HelperDepartmentServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("url", "add");
        req.getRequestDispatcher("/WEB-INF/views/departmentAddEdit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        departmentAddEdit(req, resp, "add");
    }
}
