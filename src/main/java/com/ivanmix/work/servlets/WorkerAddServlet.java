package com.ivanmix.work.servlets;

import com.ivanmix.work.entity.Worker;
import com.ivanmix.work.helper.HelperWorkerServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WorkerAddServlet extends HelperWorkerServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String pathInfo = req.getPathInfo();
        if (pathInfo != null) {
            try {
                String[] pathOptions = pathInfo.split("/");
                Long departmentId = Long.valueOf(pathOptions[1]);
                Worker worker = new Worker();
                worker.setDepartmentId(departmentId);
                worker.setAge(18);
                req.setAttribute("worker", worker);
            } catch (NumberFormatException e) {
                //ok
            }
        }
        req.setAttribute("departments", departmentService.getAllDepartments());
        req.setAttribute("formUrl", "add");
        req.getRequestDispatcher("/WEB-INF/views/workerAddEdit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        workerAddEdit(req, resp, "add");
    }
}
