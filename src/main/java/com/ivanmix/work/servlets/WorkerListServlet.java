package com.ivanmix.work.servlets;

import com.ivanmix.work.service.WorkerService;
import com.ivanmix.work.service.impl.WorkerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WorkerListServlet extends HttpServlet {
    private WorkerService workerService = new WorkerServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("workers",  workerService.getAllWorkers());
        req.getRequestDispatcher("/WEB-INF/views/workerList.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(String.format("%s/workers", req.getContextPath()));
    }
}
