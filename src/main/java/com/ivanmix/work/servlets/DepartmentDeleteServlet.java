package com.ivanmix.work.servlets;

import com.ivanmix.work.entity.Department;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.helper.HelperServlet;
import com.ivanmix.work.service.DepartmentService;
import com.ivanmix.work.service.WorkerService;
import com.ivanmix.work.service.impl.DepartmentServiceImpl;
import com.ivanmix.work.service.impl.WorkerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DepartmentDeleteServlet extends HttpServlet {
    DepartmentService departmentService = new DepartmentServiceImpl();
    WorkerService workerService = new WorkerServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long departmentId = HelperServlet.getId(req);
            Department department = departmentService.getDepartmentById(departmentId);
            Long amount = workerService.getCountOfWorkersAtTheDepartment(departmentId);
            req.setAttribute("departments", departmentService.getAllDepartments());
            req.setAttribute("department", department);
            req.setAttribute("amount",  amount);
            req.getRequestDispatcher("/WEB-INF/views/departmentDelete.jsp").forward(req, resp);
        } catch (EntityNotExist workerIsNotExist) {
            req.setAttribute("message", "Department does not exist");
            req.getRequestDispatcher("/WEB-INF/views/404.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            try {
                String select = req.getParameter("select");
                Long departmentId = Long.valueOf(req.getParameter("id"));
                if (select.equals("delete_and_change")) {
                    Long newDepartmentId = Long.valueOf(req.getParameter("newDepartmentId"));
                    workerService.changeDepartment(departmentId, newDepartmentId);
                    departmentService.delete(departmentId);
                } else if (select.equals("delete_all")) {
                    workerService.deleteByDepartmentId(departmentId);
                    departmentService.delete(departmentId);
                }
                resp.sendRedirect(String.format("%s/departments", req.getContextPath()));
            } catch (NumberFormatException e) {
                throw new EntityNotExist();
            }
        } catch (EntityNotExist workerIsNotExist) {
            req.setAttribute("message", "Department does not exist");
            req.getRequestDispatcher("/WEB-INF/views/404.jsp").forward(req, resp);
        }
    }
}
