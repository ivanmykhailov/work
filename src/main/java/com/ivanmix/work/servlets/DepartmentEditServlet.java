package com.ivanmix.work.servlets;

import com.ivanmix.work.entity.Department;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.helper.HelperDepartmentServlet;
import com.ivanmix.work.helper.HelperServlet;
import com.ivanmix.work.service.DepartmentService;
import com.ivanmix.work.service.impl.DepartmentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DepartmentEditServlet extends HelperDepartmentServlet {
    private DepartmentService departmentService = new DepartmentServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long departmentId = HelperServlet.getId(req);
            Department department = departmentService.getDepartmentById(departmentId);
            req.setAttribute("url", "edit");
            req.setAttribute("department", department);
            req.getRequestDispatcher("/WEB-INF/views/departmentAddEdit.jsp").forward(req, resp);
        } catch (EntityNotExist workerIsNotExist) {
            req.setAttribute("message", "Department does not exist");
            req.getRequestDispatcher("/WEB-INF/views/404.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        departmentAddEdit(req, resp, "edit");
    }
}
