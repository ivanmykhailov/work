package com.ivanmix.work.exception;

public class DuplicateDepartmentName extends Exception {
    public DuplicateDepartmentName() { super(); }
    public DuplicateDepartmentName(String message) { super(message); }
    public DuplicateDepartmentName(String message, Throwable cause) { super(message, cause); }
    public DuplicateDepartmentName(Throwable cause) { super(cause); }
}
