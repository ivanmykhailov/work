package com.ivanmix.work.exception;

public class DuplicateWorkerEmail extends Exception {
    public DuplicateWorkerEmail() { super(); }
    public DuplicateWorkerEmail(String message) { super(message); }
    public DuplicateWorkerEmail(String message, Throwable cause) { super(message, cause); }
    public DuplicateWorkerEmail(Throwable cause) { super(cause); }
}
