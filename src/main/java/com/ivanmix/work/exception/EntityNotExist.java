package com.ivanmix.work.exception;

public class EntityNotExist extends Exception {
    public EntityNotExist() { super(); }
    public EntityNotExist(String message) { super(message); }
    public EntityNotExist(String message, Throwable cause) { super(message, cause); }
    public EntityNotExist(Throwable cause) { super(cause); }
}
