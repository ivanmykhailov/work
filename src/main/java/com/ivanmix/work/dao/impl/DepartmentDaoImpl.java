package com.ivanmix.work.dao.impl;

import com.ivanmix.work.entity.Department;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.exception.DuplicateDepartmentName;
import com.ivanmix.work.dao.DepartmentDao;
import org.apache.log4j.Logger;
import org.h2.jdbc.JdbcSQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DepartmentDaoImpl extends AbstractDaoImpl implements DepartmentDao {
    private static final Logger LOGGER = Logger.getLogger(DepartmentDaoImpl.class);
    private static final int DUPLICATE_CODE = 23505;

    public void addDepartment(Department department) throws  DuplicateDepartmentName {
        Connection connection = JdbcConfig.getDBConnection();
        PreparedStatement insertPreparedStatement = null;
        String insertQuery = "INSERT INTO DEPARTMENT (name) VALUES (?)";

        try {
            try {
                connection.setAutoCommit(false);
                insertPreparedStatement = connection.prepareStatement(insertQuery);
                insertPreparedStatement.setString(1, department.getName());
                insertPreparedStatement.executeUpdate();
                insertPreparedStatement.close();
                connection.commit();
            } catch (JdbcSQLException e) {
                LOGGER.error(e);
                if (e.getErrorCode() == DUPLICATE_CODE) {
                    throw new DuplicateDepartmentName();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }

    public Department getDepartmentById(Long id) throws EntityNotExist {
        return getDepartment(id, "id");
    }

    public Department getDepartmentByName(String name) throws EntityNotExist {
        return getDepartment(name, "name");
    }

    public void updateDepartment(Department department) throws DuplicateDepartmentName {
        Connection connection = JdbcConfig.getDBConnection();
        PreparedStatement updatePreparedStatement = null;
        String updateQuery = "UPDATE DEPARTMENT SET name = ? WHERE id = ?";

        try {
            try {
                connection.setAutoCommit(false);
                updatePreparedStatement = connection.prepareStatement(updateQuery);
                updatePreparedStatement.setString(1, department.getName());
                updatePreparedStatement.setLong(2, department.getId());
                updatePreparedStatement.executeUpdate();
                updatePreparedStatement.close();
                connection.commit();
            } catch (JdbcSQLException e) {
                LOGGER.error(e);
                if (e.getErrorCode() == DUPLICATE_CODE) {
                    throw new DuplicateDepartmentName();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }

    public ArrayList<Department> getAllDepartments() {
        ArrayList<Department> departments = new ArrayList<Department>();
        Connection connection = JdbcConfig.getDBConnection();
        PreparedStatement selectPreparedStatement =  null;
        String selectQuery = "select * from DEPARTMENT";

        try {
            try {
                selectPreparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = selectPreparedStatement.executeQuery();
                while (rs.next()) {
                    Department department = new Department(rs.getLong("id"), rs.getString("name"));
                    departments.add(department);
                }
                selectPreparedStatement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return departments;
    }

    private Department getDepartment(Object param, String fileName) throws EntityNotExist {
        Department department = null;
        Connection connection = JdbcConfig.getDBConnection();
        PreparedStatement selectPreparedStatement =  null;
        String selectQuery = "SELECT * FROM DEPARTMENT WHERE " + fileName + " = ?";
        try {
            try {
                selectPreparedStatement = connection.prepareStatement(selectQuery);
                selectPreparedStatement.setObject(1, param);
                ResultSet rs = selectPreparedStatement.executeQuery();

                ArrayList<Department> departments = new ArrayList<Department>();
                while (rs.next()) {
                    Department d = new Department(rs.getLong("id"), rs.getString("name"));
                    departments.add(d);
                }
                selectPreparedStatement.close();
                if (departments.isEmpty()) {
                    throw new EntityNotExist();
                } else {
                    department = departments.get(0);
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return department;
    }


    public void delete(Long id) {
        delete(id, "DEPARTMENT", "id");
    }
}
