package com.ivanmix.work.dao.impl;

import com.ivanmix.work.entity.Worker;
import org.apache.log4j.Logger;
import org.h2.tools.DeleteDbFiles;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public final class JdbcConfig {
    private  JdbcConfig() { }
    private static final Logger LOGGER = Logger.getLogger(JdbcConfig.class);
    private static String databaseDriver = "";
    private static String databaseConnection = "";
    private static String databaseUser    = "";
    private static String databasePassword = "";
    private static String projectMode = "";
    private static String projectDemoData = "";

    static {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream("settings.properties");
        Properties properties = new Properties();
        try {
            properties.load(input);
            projectMode         = properties.getProperty("project.mode");
            projectDemoData     = properties.getProperty("project.demo");
            databaseDriver      = properties.getProperty("db.driver");
            databaseConnection  = properties.getProperty("db.connection");
            databaseUser        = properties.getProperty("db.user");
            databasePassword    = properties.getProperty("db.password");
        } catch (IOException e) {
            LOGGER.error(e);
            projectMode         = "development";
            databaseDriver      = "org.h2.Driver";
            databaseConnection  = "jdbc:h2:~/work";
            databaseUser        = "";
            databasePassword    = "";
        }
    }



    public static void initProject() throws SQLException {
        if (projectMode.equals("live")) {
            boolean tableExists = false;
            Connection conn = getDBConnection();
            ResultSet rset = conn.getMetaData().getTables(null, null, "WORKER", null);
            if (rset.next()) {
                tableExists = true;
            }
            if (!tableExists) {
                createDataBase();
                if (projectDemoData.equals("yes")) {
                    insertDemoData();
                }
            }
        } else {
            deleteDataBase();
            createDataBase();
            insertDemoData();
        }
    }

    public static void deleteDataBase() throws SQLException {
        DeleteDbFiles.execute("~", "work", true);
    }

    public static void createDataBase() throws SQLException {
        Connection connection = getDBConnection();
        PreparedStatement createPreparedStatement = null;

        String CreateQuery = "CREATE TABLE WORKER(id bigint auto_increment primary key, "
                + "name varchar(255), "
                + "email varchar(255) UNIQUE, "
                + "age INT, birthday DATE, "
                + "departmentId bigint NOT NULL ) CHARSET=utf8";
        String CreateDepartmentQuery = "CREATE TABLE DEPARTMENT(id bigint auto_increment primary key, "
                + "name varchar(255) UNIQUE ) CHARSET=utf8";

        try {
            connection.setAutoCommit(false);
            createPreparedStatement = connection.prepareStatement(CreateQuery);
            createPreparedStatement.executeUpdate();
            createPreparedStatement.close();

            createPreparedStatement = connection.prepareStatement(CreateDepartmentQuery);
            createPreparedStatement.executeUpdate();
            createPreparedStatement.close();

            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(e);
        } catch (Exception e) {
            LOGGER.error(e);
        } finally {
            connection.close();
        }
    }

    public static void insertDemoData() throws SQLException {
        Connection connection = getDBConnection();
        PreparedStatement insertPreparedStatement = null;

        String InsertQuery = "INSERT INTO WORKER" + "(name, email, age, birthday,departmentId) values" + "(?,?,?,?,?)";
        String InsertDepartmentQuery = "INSERT INTO DEPARTMENT" + "(name) values" + "(?)";

        try {
            connection.setAutoCommit(false);
            String departments[] = {"Designer", "Frontend", "Backend"};
            List<Worker> workers = getWorkers();

            for (String name: departments) {
                insertPreparedStatement = connection.prepareStatement(InsertDepartmentQuery);
                insertPreparedStatement.setString(1, name);
                insertPreparedStatement.executeUpdate();
                insertPreparedStatement.close();
            }

            for (Worker worker: workers) {
                insertPreparedStatement = connection.prepareStatement(InsertQuery);
                insertPreparedStatement.setString(1, worker.getName());
                insertPreparedStatement.setString(2, worker.getEmail());
                insertPreparedStatement.setInt(3, worker.getAge());
                insertPreparedStatement.setDate(4, worker.getBirthday());
                insertPreparedStatement.setLong(5, worker.getDepartmentId());
                insertPreparedStatement.executeUpdate();
                insertPreparedStatement.close();
            }
            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(e);
        } catch (Exception e) {
            LOGGER.error(e);
        } finally {
            connection.close();
        }
    }

    protected static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(databaseDriver);
        } catch (ClassNotFoundException e) {
            LOGGER.error(e);
        }

        try {
            dbConnection = DriverManager.getConnection(databaseConnection, databaseUser,
                    databasePassword);
            return dbConnection;
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return dbConnection;
    }

    private static List<Worker> getWorkers() throws ParseException {
        List<Worker> workers = new ArrayList<Worker>();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date birthday  = format.parse("1990-10-20");

        workers.add(new Worker("Olga", "olga@gmail.com", 18, new Date(birthday.getTime()), 1L));
        workers.add(new Worker("Oleg", "oleg@gmail.com", 18, new Date(birthday.getTime()), 1L));
        workers.add(new Worker("Misha", "misha@gmail.com", 18, new Date(birthday.getTime()), 2L));
        workers.add(new Worker("Oleg", "oleg_front@gmail.com", 18, new Date(birthday.getTime()), 2L));
        workers.add(new Worker("Sveta", "sveta@gmail.com", 18, new Date(birthday.getTime()), 2L));
        workers.add(new Worker("Dima", "dima@gmail.com", 18, new Date(birthday.getTime()), 2L));
        workers.add(new Worker("Peter", "peter@gmail.com", 18, new Date(birthday.getTime()), 3L));
        workers.add(new Worker("Oksana", "oksana@gmail.com", 18, new Date(birthday.getTime()), 3L));

        return workers;
    }
}
