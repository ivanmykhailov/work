package com.ivanmix.work.dao.impl;

import com.ivanmix.work.entity.Worker;
import com.ivanmix.work.exception.DuplicateWorkerEmail;
import com.ivanmix.work.dao.WorkerDao;
import com.ivanmix.work.exception.EntityNotExist;
import org.apache.log4j.Logger;
import org.h2.jdbc.JdbcSQLException;

import java.sql.*;
import java.util.ArrayList;

public class WorkerDaoImpl extends AbstractDaoImpl implements WorkerDao {
    private static final Logger LOGGER = Logger.getLogger(JdbcConfig.class);
    private static final int DUPLICATE_CODE = 23505;
    public ArrayList<Worker> getAllWorkers() {
        ArrayList<Worker> workers = new ArrayList<Worker>();
        String selectQuery = "SELECT * FROM WORKER";

        try {
            Connection connection = JdbcConfig.getDBConnection();
            PreparedStatement selectPreparedStatement = null;
            try {
                selectPreparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = selectPreparedStatement.executeQuery();
                while (rs.next()) {
                    Worker worker = new Worker(
                            rs.getLong("id"),
                            rs.getString("name"),
                            rs.getString("email"),
                            rs.getInt("age"),
                            rs.getDate("birthday"),
                            rs.getLong("departmentId")
                    );
                    workers.add(worker);
                }
                selectPreparedStatement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return workers;
    }

    public ArrayList<Worker> getWorkersByDepartmentId(Long id) {
        return getWorkers(id, "departmentId");
    }

    public Worker getWorkerById(Long id) throws EntityNotExist {
        ArrayList<Worker> workers = getWorkers(id, "id");
        if (workers.isEmpty()) {
            throw new EntityNotExist();
        } else {
            return workers.get(0);
        }
    }

    public Worker getWorkerByEmail(String email) throws EntityNotExist {
        ArrayList<Worker> workers = getWorkers(email, "email");
        if (workers.isEmpty()) {
            throw new EntityNotExist();
        } else {
            return workers.get(0);
        }
    }

    public void updateWorker(Worker worker)  throws DuplicateWorkerEmail {
        Connection connection = JdbcConfig.getDBConnection();
        PreparedStatement updatePreparedStatement = null;
        String updateQuery = "UPDATE WORKER SET name = ?, email = ?, age = ?, birthday = ?, departmentId = ? WHERE id = ?";
        try {
            try {
                connection.setAutoCommit(false);
                updatePreparedStatement = connection.prepareStatement(updateQuery);
                updatePreparedStatement.setString(1, worker.getName());
                updatePreparedStatement.setString(2, worker.getEmail());
                updatePreparedStatement.setInt(3, worker.getAge());
                updatePreparedStatement.setDate(4, worker.getBirthday());
                updatePreparedStatement.setLong(5, worker.getDepartmentId());
                updatePreparedStatement.setLong(6, worker.getId());
                updatePreparedStatement.executeUpdate();
                updatePreparedStatement.close();
                connection.commit();
            } catch (JdbcSQLException e) {
                LOGGER.error(e);
                if (e.getErrorCode() == DUPLICATE_CODE) {
                    throw new DuplicateWorkerEmail();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }

    public void addWorker(Worker worker) throws DuplicateWorkerEmail {
        String InsertQuery = "INSERT INTO WORKER" + "(name, email, age, birthday, departmentId) values" + "(?,?,?,?,?)";
        try {
            Connection connection = JdbcConfig.getDBConnection();
            PreparedStatement insertPreparedStatement = null;

            try {
                connection.setAutoCommit(false);
                insertPreparedStatement = connection.prepareStatement(InsertQuery);
                insertPreparedStatement.setString(1, worker.getName());
                insertPreparedStatement.setString(2, worker.getEmail());
                insertPreparedStatement.setInt(3, worker.getAge());
                insertPreparedStatement.setDate(4, worker.getBirthday());
                insertPreparedStatement.setLong(5, worker.getDepartmentId());
                insertPreparedStatement.executeUpdate();
                insertPreparedStatement.close();

                connection.commit();
            } catch (JdbcSQLException e) {
                LOGGER.error(e);
                if (e.getErrorCode() == DUPLICATE_CODE) {
                    throw new DuplicateWorkerEmail();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }

    public void delete(Long id) {
        delete(id, "WORKER", "id");
    }

    public void deleteByDepartmentId(Long id) {
        delete(id, "WORKER", "departmentId");
    }

    public Long getCountOfWorkersAtTheDepartment(Long id) {
        int count = getWorkers(id, "departmentId").size();
        return  (long) count;
    }

    public void changeDepartment(Long oldId, Long newId) {
        Connection connection = JdbcConfig.getDBConnection();
        PreparedStatement updatePreparedStatement = null;
        String updateQuery = "UPDATE WORKER set departmentId = ? WHERE departmentId = ?";
        try {
            try {
                connection.setAutoCommit(false);
                updatePreparedStatement = connection.prepareStatement(updateQuery);
                updatePreparedStatement.setLong(1, newId);
                updatePreparedStatement.setLong(2, oldId);
                updatePreparedStatement.executeUpdate();
                updatePreparedStatement.close();
                connection.commit();
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }

    }

    private ArrayList<Worker> getWorkers(Object param, String fieldName) {
        ArrayList<Worker> workers = new ArrayList<Worker>();
        String selectQuery = "SELECT * FROM WORKER WHERE " + fieldName + " = ?";
        try {
            Connection connection = JdbcConfig.getDBConnection();
            PreparedStatement selectPreparedStatement = null;
            try {
                selectPreparedStatement = connection.prepareStatement(selectQuery);
                selectPreparedStatement.setObject(1, param);
                ResultSet rs = selectPreparedStatement.executeQuery();
                while (rs.next()) {
                    Worker worker = new Worker(
                            rs.getLong("id"),
                            rs.getString("name"),
                            rs.getString("email"),
                            rs.getInt("age"),
                            rs.getDate("birthday"),
                            rs.getLong("departmentId")
                    );
                    workers.add(worker);
                }
                selectPreparedStatement.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return workers;
    }
}
