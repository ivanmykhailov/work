package com.ivanmix.work.dao.impl;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class AbstractDaoImpl {
    private static final Logger LOGGER = Logger.getLogger(AbstractDaoImpl.class);

    public void delete(Long id, String tableName, String fieldName) {
        Connection connection = JdbcConfig.getDBConnection();
        PreparedStatement deletePreparedStatement = null;
        String deleteQuery = "DELETE " + tableName + " WHERE " + fieldName + " = ?";
        try {
            try {
                connection.setAutoCommit(false);
                deletePreparedStatement = connection.prepareStatement(deleteQuery);
                deletePreparedStatement.setLong(1, id);
                deletePreparedStatement.executeUpdate();
                deletePreparedStatement.close();
                connection.commit();
            } catch (SQLException e) {
                LOGGER.error(e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }
}
