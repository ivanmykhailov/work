package com.ivanmix.work.dao;

import com.ivanmix.work.entity.Department;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.exception.DuplicateDepartmentName;

import java.util.ArrayList;

public interface DepartmentDao {
    void addDepartment(Department department) throws DuplicateDepartmentName;
    Department getDepartmentById(Long id) throws EntityNotExist;
    Department getDepartmentByName(String name) throws EntityNotExist;
    void updateDepartment(Department department) throws DuplicateDepartmentName;
    ArrayList<Department> getAllDepartments();
    void delete(Long id);
}
