package com.ivanmix.work.helper;

import com.ivanmix.work.exception.DuplicateWorkerEmail;
import com.ivanmix.work.service.DepartmentService;
import com.ivanmix.work.service.WorkerService;
import com.ivanmix.work.service.impl.DepartmentServiceImpl;
import com.ivanmix.work.service.impl.WorkerServiceImpl;
import com.ivanmix.work.validation.WorkerValidation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelperWorkerServlet extends HttpServlet {
    protected WorkerService workerService = new WorkerServiceImpl();
    protected DepartmentService departmentService = new DepartmentServiceImpl();

    public void workerAddEdit(HttpServletRequest req, HttpServletResponse resp, String operator)
            throws IOException, ServletException {
        WorkerValidation worker = new WorkerValidation().validation(req);
        if (worker.getErrors().isEmpty()) {
            try {
                if (operator.equals("add")) {
                    workerService.addWorker(worker);
                    resp.sendRedirect(String.format("%s/department/%s", req.getContextPath(), worker.getDepartmentId()));
                } else if (operator.equals("edit")) {
                    workerService.updateWorker(worker);
                    resp.sendRedirect(String.format("%s/department/%s", req.getContextPath(), worker.getDepartmentId()));
                }
            } catch (DuplicateWorkerEmail duplicateWorkerEmail) {
                req.setAttribute("worker", worker);
                req.setAttribute("departments", departmentService.getAllDepartments());
                worker.getErrors().add("Sorry, but that email is already taken. Choose another email.");
                req.setAttribute("errors", worker.getErrors());
                req.setAttribute("formUrl", operator);
                req.getRequestDispatcher("/WEB-INF/views/workerAddEdit.jsp").forward(req, resp);
            }
            return;
        }
        req.setAttribute("worker", worker);
        req.setAttribute("departments", departmentService.getAllDepartments());
        req.setAttribute("errors", worker.getErrors());
        req.setAttribute("formUrl", operator);
        req.getRequestDispatcher("/WEB-INF/views/workerAddEdit.jsp").forward(req, resp);
    }
}
