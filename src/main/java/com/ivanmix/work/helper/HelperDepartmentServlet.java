package com.ivanmix.work.helper;

import com.ivanmix.work.exception.DuplicateDepartmentName;
import com.ivanmix.work.service.DepartmentService;
import com.ivanmix.work.service.impl.DepartmentServiceImpl;
import com.ivanmix.work.validation.DepartmentValidation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelperDepartmentServlet extends HttpServlet {
    protected DepartmentService departmentService = new DepartmentServiceImpl();

    public void departmentAddEdit(HttpServletRequest req, HttpServletResponse resp, String operator)
            throws IOException, ServletException {
        DepartmentValidation department = new DepartmentValidation().validation(req);

        if (department.getErrors().isEmpty()) {
            try {
                if (operator.equals("add")) {
                    departmentService.addDepartment(department);
                } else if (operator.equals("edit")) {
                    departmentService.updateDepartment(department);
                }
                resp.sendRedirect(String.format("%s/departments", req.getContextPath()));
            } catch (DuplicateDepartmentName duplicateDepartmentName) {
                req.setAttribute("department", department);
                department.getErrors().add("Sorry, but that name is already taken. Choose another name.");
                req.setAttribute("errors", department.getErrors());
                req.setAttribute("url", operator);
                req.getRequestDispatcher("/WEB-INF/views/departmentAddEdit.jsp").forward(req, resp);
            }
            return;
        }
        req.setAttribute("department", department);
        req.setAttribute("errors", department.getErrors());
        req.setAttribute("url", operator);
        req.getRequestDispatcher("/WEB-INF/views/departmentAddEdit.jsp").forward(req, resp);
    }
}
