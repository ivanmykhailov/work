package com.ivanmix.work.helper;

import com.ivanmix.work.validation.Validation;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class HelperValidation {
    private static final Logger LOGGER = Logger.getLogger(HelperValidation.class);
    private static final int MIN_NAME_LENGTH = 3;

    public void addId(Validation validation, HttpServletRequest req) {
        if (!req.getParameter("id").equals("")) {
            try {
                Long id = Long.valueOf(req.getParameter("id"));
                validation.setId(id);
            } catch (NumberFormatException e) {
                LOGGER.error(e);
                validation.getErrors().add("Field Id can not be a number");
            }
        }
    }

    public void addName(Validation validation, HttpServletRequest req) {
        String name = req.getParameter("name");
        if (name.length() < MIN_NAME_LENGTH) {
            validation.getErrors().add("Name should be more than 2 characters");
        }
        validation.setName(name);
    }

}
