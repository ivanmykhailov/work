package com.ivanmix.work.helper;

import com.ivanmix.work.exception.EntityNotExist;
import javax.servlet.http.HttpServletRequest;

public final class HelperServlet {
    private HelperServlet() { }
    public static Long getId(HttpServletRequest req) throws EntityNotExist {
        Long id = 0L;
        try {
            String pathInfo = req.getPathInfo();
            String[] pathOptions = pathInfo.split("/");
            id = Long.valueOf(pathOptions[1]);
        } catch (NumberFormatException e) {
            throw new EntityNotExist();
        }
        return id;
    }
}
