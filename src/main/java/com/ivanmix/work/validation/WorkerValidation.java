package com.ivanmix.work.validation;

import com.ivanmix.work.entity.Worker;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.helper.HelperValidation;
import com.ivanmix.work.service.WorkerService;
import com.ivanmix.work.service.impl.WorkerServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class WorkerValidation extends Worker implements Validation {
    private static final Logger LOGGER = Logger.getLogger(WorkerValidation.class);
    private static final int ADULT_AGE = 18;
    private WorkerService workerService = new WorkerServiceImpl();
    private ArrayList<String> errors = new ArrayList<String>();

    public ArrayList<String> getErrors() {
        return errors;
    }

    public WorkerValidation validation(HttpServletRequest req) {
        WorkerValidation worker = new WorkerValidation();
        worker = addWorker(worker, req);
        return worker;
    }

    private WorkerValidation addWorker(WorkerValidation worker, HttpServletRequest req) {
        worker.setId(0L);
        worker.setName("");
        worker.setEmail("");
        worker.setAge(0);
        worker.setBirthday(null);
        worker.setDepartmentId(0L);

        HelperValidation helper = new HelperValidation();
        helper.addId(worker, req);
        helper.addName(worker, req);

        addEmail(worker, req);
        addAge(worker, req);
        addBirthday(worker, req);
        addDepartment(worker, req);

        return worker;
    }

    private void addEmail(WorkerValidation worker, HttpServletRequest req) {
        String email = req.getParameter("email");
        if (isValidEmailAddress(email)) {
            try {
                Worker w = workerService.getWorkerByEmail(email);
                if (w.getId() != worker.getId()) {
                    worker.errors.add("Sorry, but that email is already taken. Choose another email.");
                }
            } catch (EntityNotExist workerIsNotExist) {
                //OK
            }
            worker.setEmail(email);
            return;
        }
        worker.errors.add("Worker email is not valid");
    }

    private void addAge(WorkerValidation worker, HttpServletRequest req) {
        if (!req.getParameter("age").equals("")) {
            try {
                int age = Integer.parseInt(req.getParameter("age"));
                if (age < ADULT_AGE) {
                    worker.errors.add("The worker must be an adult");
                }
                worker.setAge(age);
            } catch (NumberFormatException e) {
                LOGGER.error(e);
                worker.errors.add("Field Age can not be a number");
            }
            return;
        }
        worker.errors.add("Age required field");
    }

    private void addBirthday(WorkerValidation worker, HttpServletRequest req) {
        Date birthday;
        String stringDate;
        if (!req.getParameter("birthday").equals("")) {
            stringDate = req.getParameter("birthday");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date parsed = null;
            try {
                parsed = format.parse(stringDate);
            } catch (ParseException e) {
                LOGGER.error(e);
                worker.errors.add("Enter the date in a different format");
            }
            birthday = new Date(parsed.getTime());
            worker.setBirthday(birthday);
            return;
        }
        worker.errors.add("Birthday required field");
    }

    private void addDepartment(WorkerValidation worker, HttpServletRequest req) {
        if (!req.getParameter("departmentId").equals("")) {
            try {
                Long departmentId = Long.valueOf(req.getParameter("departmentId"));
                worker.setDepartmentId(departmentId);
            } catch (NumberFormatException e) {
                LOGGER.error(e);
                worker.errors.add("Field Department Id can not be a number");
            }
            return;
        }
        worker.errors.add("Department required field");
    }

    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\."
        + "[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}

