package com.ivanmix.work.validation;

import java.util.ArrayList;

public interface Validation {
    void setId(Long id);
    void setName(String name);
    ArrayList<String> getErrors();
}
