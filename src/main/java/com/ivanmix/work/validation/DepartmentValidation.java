package com.ivanmix.work.validation;

import com.ivanmix.work.entity.Department;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.helper.HelperValidation;
import com.ivanmix.work.service.DepartmentService;
import com.ivanmix.work.service.impl.DepartmentServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class DepartmentValidation extends Department implements Validation {
    private DepartmentService departmentService = new DepartmentServiceImpl();

    private ArrayList<String> errors = new ArrayList<String>();

    public ArrayList<String> getErrors() {
        return errors;
    }

    public DepartmentValidation validation(HttpServletRequest req) {
        DepartmentValidation department = new DepartmentValidation();

        HelperValidation helper = new HelperValidation();
        helper.addId(department, req);
        helper.addName(department, req);

        try {
            Department d = departmentService.getDepartmentByName(department.getName());
            if (d.getId() != department.getId()) {
                department.errors.add("Sorry, but that name is already taken. Choose another name.");
            }
        } catch (EntityNotExist departmentNotExist) {
            //OK
        }
        return department;
    }
}
