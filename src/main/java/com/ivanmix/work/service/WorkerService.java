package com.ivanmix.work.service;

import com.ivanmix.work.entity.Worker;
import com.ivanmix.work.exception.DuplicateWorkerEmail;
import com.ivanmix.work.exception.EntityNotExist;

import java.util.ArrayList;

public interface WorkerService {
    ArrayList<Worker> getAllWorkers();
    ArrayList<Worker> getWorkersByDepartmentId(Long id);
    Worker getWorkerById(Long id) throws EntityNotExist;
    Worker getWorkerByEmail(String email) throws EntityNotExist;
    void addWorker(Worker worker) throws DuplicateWorkerEmail;
    void updateWorker(Worker worker) throws DuplicateWorkerEmail;
    void delete(Long id);
    void deleteByDepartmentId(Long id);
    Long getCountOfWorkersAtTheDepartment(Long id);
    void changeDepartment(Long oldId, Long newId);
}
