package com.ivanmix.work.service.impl;

import com.ivanmix.work.dao.WorkerDao;
import com.ivanmix.work.entity.Worker;
import com.ivanmix.work.exception.DuplicateWorkerEmail;
import com.ivanmix.work.dao.impl.WorkerDaoImpl;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.service.WorkerService;

import java.util.ArrayList;

public class WorkerServiceImpl implements WorkerService {
    private WorkerDao workerDao = new WorkerDaoImpl();

    public ArrayList<Worker> getAllWorkers() {
        ArrayList<Worker> workers = new ArrayList<Worker>();
        workers = workerDao.getAllWorkers();
        return workers;
    }

    public ArrayList<Worker> getWorkersByDepartmentId(Long id) {
        ArrayList<Worker> workers = new ArrayList<Worker>();
        workers = workerDao.getWorkersByDepartmentId(id);
        return workers;
    }

    public Worker getWorkerById(Long id) throws EntityNotExist {
        return workerDao.getWorkerById(id);
    }

    public Worker getWorkerByEmail(String email) throws EntityNotExist {
        return workerDao.getWorkerByEmail(email);
    }

    public void addWorker(Worker worker) throws DuplicateWorkerEmail {
        workerDao.addWorker(worker);
    }

    public void updateWorker(Worker worker) throws DuplicateWorkerEmail {
        workerDao.updateWorker(worker);
    }

    public void delete(Long id) {
        workerDao.delete(id);
    }

    public void deleteByDepartmentId(Long id) {
        workerDao.deleteByDepartmentId(id);
    }

    public Long getCountOfWorkersAtTheDepartment(Long id) {
        return workerDao.getCountOfWorkersAtTheDepartment(id);
    }

    public void changeDepartment(Long oldId, Long newId) {
        workerDao.changeDepartment(oldId, newId);
    }
}
