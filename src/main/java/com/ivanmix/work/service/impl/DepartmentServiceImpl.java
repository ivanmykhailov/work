package com.ivanmix.work.service.impl;

import com.ivanmix.work.dao.DepartmentDao;
import com.ivanmix.work.entity.Department;
import com.ivanmix.work.exception.EntityNotExist;
import com.ivanmix.work.exception.DuplicateDepartmentName;
import com.ivanmix.work.dao.impl.DepartmentDaoImpl;
import com.ivanmix.work.service.DepartmentService;

import java.util.ArrayList;

public class DepartmentServiceImpl implements DepartmentService {
    private DepartmentDao departmentDao = new DepartmentDaoImpl();

    public void addDepartment(Department department) throws DuplicateDepartmentName {
        departmentDao.addDepartment(department);
    }

    public Department getDepartmentById(Long id) throws EntityNotExist {
        return departmentDao.getDepartmentById(id);
    }

    public Department getDepartmentByName(String name) throws EntityNotExist {
        return departmentDao.getDepartmentByName(name);
    }

    public void updateDepartment(Department department) throws DuplicateDepartmentName {
        departmentDao.updateDepartment(department);
    }

    public ArrayList<Department> getAllDepartments() {
        ArrayList<Department> departments = new ArrayList<Department>();
        departments = departmentDao.getAllDepartments();
        return departments;
    }

    public void delete(Long id) {
        departmentDao.delete(id);
    }
}
