package com.ivanmix.work.filter;

import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.FilterChain;
import javax.servlet.ServletResponse;
import java.io.IOException;


public class ErrorFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(ErrorFilter.class);

        public void  init(FilterConfig config) { }
        public void  doFilter(ServletRequest request,
                              ServletResponse response,
                              FilterChain chain) throws IOException, ServletException {
            try {
                chain.doFilter(request, response);
            } catch (Exception e) {
                LOGGER.equals(e);
                request.setAttribute("message", e);
                request.getRequestDispatcher("/WEB-INF/views/404.jsp").forward(request, response);
            }
        }
        public void destroy() { }
}
