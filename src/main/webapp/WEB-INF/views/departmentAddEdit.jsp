<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${url} Department</title>
    <%@include file="blocks/head.jsp" %>
</head>
<body>
    <div id="wrapper">
        <div class="wrap">
            <%@include file="blocks/header.jsp" %>
            <div id="main">
                <form action="${pageContext.servletContext.contextPath}/department-${url}" method="post">
                    <c:forEach var="error" items="${errors}">
                        <div class="error_box">${error}</div>
                    </c:forEach>
                    <table class="table">
                        <tr>
                            <th><label for="name">Name</label></th>
                            <td><input id="name" name="name" value="${department.getName()}" type="text"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" value="submit" /></td>
                        </tr>
                    </table>
                    <input type="hidden" name="id" value="${department.getId()}">
                </form>
            </div>
        </div>
        <%@include file="blocks/footer.jsp" %>
    </div>
</body>
</html>
