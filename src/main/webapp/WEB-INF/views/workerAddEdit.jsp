<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>${formUrl} Worker</title>
    <%@include file="blocks/head.jsp" %>
</head>
<body>
    <div id="wrapper">
        <div class="wrap">
            <%@include file="blocks/header.jsp" %>
            <div id="main">
                <form action="${pageContext.servletContext.contextPath}/worker-${formUrl}" method="post">
                    <c:forEach var="error" items="${errors}">
                        <div class="error_box">${error}</div>
                    </c:forEach>
                    <table class="table">
                        <tr>
                            <th>Department</th>
                            <th>
                                <select name="departmentId">
                                    <option>Select One</option>
                                    <c:forEach var="department" items="${departments}">
                                        <option
                                                <c:if test="${worker.getDepartmentId() == department.getId()}">
                                                    selected
                                                </c:if>
                                                value="${department.getId()}">${department.getName()}</option>
                                    </c:forEach>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <th><label for="email">Email</label></th>
                            <td><input id="email" name="email" value="${worker.getEmail()}" type="text"></td>
                        </tr>
                        <tr>
                            <th><label for="name">Name</label></th>
                            <td><input id="name" name="name" value="${worker.getName()}" type="text"></td>
                        </tr>
                        <tr>
                            <th><label for="age">Age</label></th>
                            <td><input id="age" name="age" value="${worker.getAge()}" type="text"></td>
                        </tr>
                        <tr>
                            <th><label for="birthday">Birthday</label></th>
                            <td><input id="birthday" name="birthday" value="${worker.getBirthday()}" type="text"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" value="submit" /></td>
                        </tr>
                    </table>
                    <input name="id" value="${worker.getId()}" type="hidden">
                </form>
            </div>
        </div>
        <%@include file="blocks/footer.jsp" %>
    </div>
</body>
</html>
