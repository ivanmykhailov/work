<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>404</title>
    <meta name="robots" content="noindex,nofollow">
    <%@include file="blocks/head.jsp" %>
</head>
<body>
    <div id="wrapper">
        <div class="wrap">
            <%@include file="blocks/header.jsp" %>
            <div id="main" class="page_404">
                <h2>The page you are looking for can't be found.</h2>
                <h1>404</h1>
                <h2>${message}</h2>
            </div>
        </div>
        <%@include file="blocks/footer.jsp" %>
    </div>
</body>
</html>
