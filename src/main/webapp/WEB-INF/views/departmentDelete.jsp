<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Department Delete</title>
    <%@include file="blocks/head.jsp" %>
</head>
<body>
    <div id="wrapper">
        <div class="wrap">
            <%@include file="blocks/header.jsp" %>
            <div id="main">
                <table class="table">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Amount workers</th>
                    </tr>
                    <tr>
                        <td><c:out value="${department.getId()}"/></td>
                        <td><c:out value="${department.getName()}"/></td>
                        <td><c:out value="${amount}"/></td>
                    </tr>
                </table>
                <form action="${pageContext.servletContext.contextPath}/department-delete" method="post">
                    <table class="table">
                        <tr>
                            <th width="33%">Cancel</th>
                            <th width="33%">
                                <c:if test="${departments.size() > 1}">
                                    Delete And Change workers department
                                </c:if>
                            </th>
                            <th width="33%">
                                <c:if test="${amount > 0}">
                                    Delete with Worker
                                </c:if>
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" name="select" value="cancel" id="select_cancel" checked>
                                <label for="select_cancel">Cancel</label>
                            </td>
                            <td>
                                <c:if test="${departments.size() > 1}">
                                    <input type="radio" name="select" value="delete_and_change" id="select_delete_and_change">
                                    <label for="select_delete_and_change">Delete And Change workers department</label>
                                    <hr>
                                    <select name="newDepartmentId">
                                        <c:forEach var="d" items="${departments}">
                                            <c:if test="${d.getId() != department.getId()}">
                                                <option value="${d.getId()}">${d.getName()}</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </c:if>
                            </td>
                            <td>
                                <c:if test="${amount > 0}">
                                    <input type="radio" name="select" value="delete_all" id="select_delete_all">
                                    <label for="select_delete_all">Delete with Worker</label>
                                </c:if>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><input type="submit" value="submit" /></td>
                        </tr>
                    </table>
                    <input type="hidden" name="id" value="${department.getId()}">
                </form>
            </div>
        </div>
        <%@include file="blocks/footer.jsp" %>
    </div>
</body>
</html>
