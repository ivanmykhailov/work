<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="header">
    <ul id="nav">
        <li><a href="<%=request.getContextPath()%>/">Home</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/departments">All Departments</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/department-add">Add Department</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/workers">All Workers</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/worker-add">Add Worker</a></li>
        <%--<li><a href="${pageContext.servletContext.contextPath}/console">H2 Console</a></li>--%>
    </ul>
</div>
