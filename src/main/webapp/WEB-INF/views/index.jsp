<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Work</title>
    <%@include file="blocks/head.jsp" %>
</head>
<body>
    <div id="wrapper">
        <div class="wrap">
            <%@include file="blocks/header.jsp" %>
            <div id="main">
                <table class="table">
                    <tr>
                        <th>Name</th>
                        <th>Add Worker</th>
                        <th>Edit</th>
                        <th>Workers List</th>
                        <th>Delete</th>
                    </tr>
                    <c:forEach var="department" items="${departments}">
                        <tr>
                            <th><c:out value="${department.getName()}"/></th>
                            <td><a href="${pageContext.servletContext.contextPath}/worker-add/${department.getId()}">Add Worker</a></td>
                            <td><a href="${pageContext.servletContext.contextPath}/department-edit/${department.getId()}">Edit</a></td>
                            <td><a href="${pageContext.servletContext.contextPath}/department/${department.getId()}">Workers List</a></td>
                            <td><a href="${pageContext.servletContext.contextPath}/department-delete/${department.getId()}">Delete</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
        <%@include file="blocks/footer.jsp" %>
    </div>
</body>
</html>
