<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Worker List</title>
    <%@include file="blocks/head.jsp" %>
</head>
<body>
    <div id="wrapper">
        <div class="wrap">
            <%@include file="blocks/header.jsp" %>
            <div id="main">
                <table class="table">
                    <tr>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Birthday</th>
                        <th>Department</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    <c:forEach var="worker" items="${workers}">
                        <tr>
                            <th><c:out value="${worker.getEmail()}"/></th>
                            <th><c:out value="${worker.getName()}"/></th>
                            <td><c:out value="${worker.getAge()}"/></td>
                            <td><c:out value="${worker.getBirthday()}"/></td>
                            <td><c:out value="${worker.getDepartmentId()}"/></td>
                            <td><a href="${pageContext.servletContext.contextPath}/worker-edit/${worker.getId()}">Edit</a></td>
                            <td><a href="${pageContext.servletContext.contextPath}/worker-delete/${worker.getId()}">Delete</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
        <%@include file="blocks/footer.jsp" %>
    </div>
</body>
</html>
