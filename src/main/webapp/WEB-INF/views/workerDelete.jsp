<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Worker Delete</title>
    <%@include file="blocks/head.jsp" %>
</head>
<body>
    <div id="wrapper">
        <div class="wrap">
            <%@include file="blocks/header.jsp" %>
            <div id="main">
                <table class="table">
                    <tr>
                        <th>Id</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Birthday</th>
                    </tr>
                    <tr>
                        <th><c:out value="${worker.getId()}"/></th>
                        <th><c:out value="${worker.getEmail()}"/></th>
                        <th><c:out value="${worker.getName()}"/></th>
                        <td><c:out value="${worker.getAge()}"/></td>
                        <td><c:out value="${worker.getBirthday()}"/></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a href="<%=request.getContextPath()%>/">Cancel</a>
                        </td>
                        <td></td>
                        <td colspan="2">
                            <form action="${pageContext.servletContext.contextPath}/worker-delete" method="post">
                                <input type="hidden" name="id" value="${worker.getId()}">
                                <input type="submit" value="Yes">
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%@include file="blocks/footer.jsp" %>
    </div>
</body>
</html>
