$(function() {
    $('#birthday').datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        minDate: "-60Y",
        maxDate: "0D"
    });
});
